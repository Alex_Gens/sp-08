package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.Task;
import ru.kazakov.iteco.model.User;

public interface IDomainService {

    @Nullable
    public ProjectDTO getProjectDTO(@Nullable final Project project);

    @Nullable
    public Project getProjectFromDTO(@Nullable final ProjectDTO dto) throws Exception;

    @Nullable
    public TaskDTO getTaskDTO(@Nullable final Task task);

    @Nullable
    public Task getTaskFromDTO(@Nullable final TaskDTO dto) throws Exception;

    @Nullable
    public UserDTO getUserDTO(@Nullable final User user);

    @Nullable
    public User getUserFromDTO(@Nullable final UserDTO dto);

}
