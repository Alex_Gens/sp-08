package ru.kazakov.iteco.api.service;

import ru.kazakov.iteco.model.Role;

public interface IRoleService {

    public void save(Role role);

}
