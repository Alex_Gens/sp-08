package ru.kazakov.iteco.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kazakov.iteco.model.Role;

public interface IRoleRepository extends JpaRepository<Role, String> {}
