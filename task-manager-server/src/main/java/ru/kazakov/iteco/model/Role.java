package ru.kazakov.iteco.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_role")
public class Role extends AbstractEntity{

    @Nullable
    @ManyToOne
    private User user;

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType role = RoleType.USER;

    @Override
    public String toString() {
        return role.toString();
    }

}
