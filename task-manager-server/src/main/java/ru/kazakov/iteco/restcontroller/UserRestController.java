package ru.kazakov.iteco.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.UserDTO;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class UserRestController {

    @NotNull
    @Autowired
    IUserService userService;

    @NotNull
    @Autowired
    IDomainService domainService;

    @PostMapping(value = "/users/create")
    public void create(
            @Nullable final String username,
            @Nullable final String password
    ) throws Exception {
        userService.create(username, password);
    }

    @GetMapping(value = "/users/{username}")
    public UserDTO findUserByUsername(@PathVariable @Nullable final String username) throws Exception {
        return domainService.getUserDTO(userService.findByUsername(username));
    }

    @GetMapping(value = "/admin/all_users")
    public List<UserDTO> findAllUsers() {
        return userService.findAllByOrderByDateCreate().stream()
                .map(v -> domainService.getUserDTO(v)).collect(Collectors.toList());
    }

    @PutMapping(value = "admin/update_user/{id}")
    public void updateUser(@Nullable final UserDTO user) throws Exception {
        userService.save(domainService.getUserFromDTO(user));
    }

    @DeleteMapping(value = "admin/user_remove/{id}")
    public void deleteUser(@PathVariable @Nullable final String id) throws Exception {
        userService.deleteById(id);
    }

}
