package ru.kazakov.iteco.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.model.Task;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/tasks",
                produces =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class TaskRestController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private IUserService userService;

    @GetMapping
    public List<TaskDTO> getAllTasks(@Nullable final String currentUserId) throws Exception {
        return taskService.findAllByUserIdOrderByDateCreate(currentUserId)
                .stream().map(v -> domainService.getTaskDTO(v)).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public TaskDTO getTask(@Nullable final String currentUserId,
                           @PathVariable @Nullable final String id
    ) throws Exception {
        @Nullable final Task task = taskService.findOne(id, currentUserId);
        if (task == null) return null;
        if (!task.getUser().getId().equals(currentUserId)) throw new Exception();
        return domainService.getTaskDTO(task);
    }

    @PostMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<TaskDTO> addTask(@Nullable final String currentUserId,
                                           @RequestBody @Nullable final TaskDTO dto
    ) throws Exception {
        if (dto == null) return null;
        dto.setUserId(currentUserId);
        @Nullable final Task task = domainService.getTaskFromDTO(dto);
        if (task == null) return null;
        @NotNull final Task newTask = taskService.save(task);
        @NotNull final ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentContextPath();
        @NotNull final UriComponents uriComponents = builder.path("/tasks/{id}").buildAndExpand(dto.getId());
        @NotNull final URI uri = uriComponents.toUri();
        @NotNull final ResponseEntity<TaskDTO> responseEntity = ResponseEntity.created(uri).body(dto);
        return responseEntity;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTask(@Nullable final String currentUserId,
                           @PathVariable @Nullable final String id
    ) throws Exception {
        @Nullable final Task task = taskService.findOne(id, currentUserId);
        if (task == null) throw new Exception();
        if (!task.getUser().getId().equals(currentUserId)) throw new Exception();
        taskService.deleteById(id);
    }

    @PutMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void updateTask(@Nullable final String currentUserId,
                           @RequestBody @Nullable final TaskDTO dto
    ) throws Exception {
        @Nullable final Task task = domainService.getTaskFromDTO(dto);
        taskService.save(task);
    }

}