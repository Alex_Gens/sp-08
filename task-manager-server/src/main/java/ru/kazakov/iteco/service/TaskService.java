package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.model.Task;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    public Task save(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception();
        return repository.save(task);
    }

    public Task findById(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return repository.findById(id).orElse(null);
    }

    public Task findOne(@Nullable final String id,
                        @Nullable final String userId
    ) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        return repository.findByIdAndUserId(id, userId);
    }

    @NotNull
    public List<Task> findAllByUserIdOrderByDateCreate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return repository.findAllByUserIdOrderByDateCreate(userId);
    }

    public void deleteById(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        repository.deleteById(id);
    }

    public void delete(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception();
        repository.delete(task);
    }

}
