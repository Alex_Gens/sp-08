package iteco.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kazakov.iteco.TaskManagerApplication;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.Task;
import ru.kazakov.iteco.model.User;
import java.util.Date;
import java.util.List;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskManagerApplication.class)
public class TaskRepositoryTest {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IRoleService roleService;

    private User testUser;

    @Before
    public void initTestUsers() throws Exception {
        if (userService.findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            user.setPassword("pass");
            testUser = userService.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
    }

    @After
    public void deleteTestUsers() throws Exception {
        Assert.assertNotNull(testUser);
        userService.deleteById(testUser.getId());
    }

    @NotNull
    private Task getTaskInstance() {
        @NotNull final Task task = new Task();
        task.setId("taskTestId");
        task.setName("taskTestName");
        task.setUser(testUser);
        task.setDateCreate(new Date());
        return task;
    }

    @Test
    public void save() {
        @NotNull final Task task = getTaskInstance();
        @Nullable final Task beforeSave = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNull(beforeSave);
        taskRepository.save(task);
        @Nullable final Task afterSave = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNotNull(afterSave);
        Assert.assertEquals(task.getId(), afterSave.getId());
    }

    @Test
    public void findById() {
        @NotNull final Task task = getTaskInstance();
        taskRepository.save(task);
        @Nullable final Task found = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNotNull(found);
        Assert.assertEquals(task.getId(), found.getId());
    }

    @Test
    public void findByIdAndUserId() {
        @NotNull final Task task = getTaskInstance();
        taskRepository.save(task);
        @Nullable final Task foundWrong = taskRepository.findByIdAndUserId(task.getId(), "wrongUserId");
        Assert.assertNull(foundWrong);
        @Nullable final Task found = taskRepository.findByIdAndUserId(task.getId(), testUser.getId());
        Assert.assertNotNull(found);
        Assert.assertEquals(task.getId(), found.getId());
    }

    @Test
    public void findAllByUserIdOrderByDateCreate() {
        @NotNull final Task task = getTaskInstance();
        @NotNull final Task secondTask = getTaskInstance();
        secondTask.setId("secondId");
        secondTask.setName("secondTask");
        taskRepository.save(task);
        taskRepository.save(secondTask);
        @NotNull final List<Task> empty = taskRepository.findAllByUserIdOrderByDateCreate("wrongUserId");
        Assert.assertEquals(0, empty.size());
        @NotNull final List<Task> tasks = taskRepository.findAllByUserIdOrderByDateCreate(testUser.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void deleteById() {
        @NotNull final Task task = getTaskInstance();
        taskRepository.save(task);
        @Nullable final Task beforeDelete = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNotNull(beforeDelete);
        taskRepository.deleteById(task.getId());
        @Nullable final Task afterDelete = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNull(afterDelete);
    }

    @Test
    public void delete() {
        @NotNull final Task task = getTaskInstance();
        taskRepository.save(task);
        @Nullable final Task beforeDelete = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNotNull(beforeDelete);
        taskRepository.delete(task);
        @Nullable final Task afterDelete = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNull(afterDelete);
    }
    
}
