External repository
------------------

[gitlab.com](https://gitlab.com/Alex_Gens/sp-08)

System requirements
-------------------

Java Development Kit v.1.8.0_201

Maven 4.0.0

Software
-------
Java 8

Spring-boot

Maven 4

PostgreSql

Developers
---------

name: Kazakov Alexey

email: aleks25000@gmail.com

Build application
-----------------
    mvn clean

    mvn install

Run application
-------------

    cd eureka-service
    mvn spring-boot:run
    
    cd configuration-server
    mvn spring-boot:run
    
    cd zuul-gateway
    mvn spring-boot:run
    
    cd task-manager-server
    mvn spring-boot:run
    
    cd task-manager-client
    mvn spring-boot:run



