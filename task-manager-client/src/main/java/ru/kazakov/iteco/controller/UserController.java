package ru.kazakov.iteco.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.client.Client;
import ru.kazakov.iteco.dto.UserDTO;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class UserController {

    @Autowired
    private Client client;

    @GetMapping("/login")
    public String login(@NotNull final Model model,
                        @Nullable final String error,
                        @Nullable final String logout
    ) {
        if (error != null) {
            model.addAttribute("error", "Username or password is incorrect.");
        }
        if (logout != null) {
            model.addAttribute("message", "Logged out successfully.");
        }
        @NotNull final String authStatus =
                SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        model.addAttribute("isAuth", !authStatus.equals("anonymousUser"));
        return "login/login";
    }

    @GetMapping("/registration")
    public String registration(@NotNull final Model model) {
        @NotNull final UserDTO user = new UserDTO();
        model.addAttribute("user", user);
        @NotNull final String authStatus =
                SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        model.addAttribute("isAuth", !authStatus.equals("anonymousUser"));
        return "login/registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") @Nullable final UserDTO user,
                               @NotNull final BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) return "login/registration";
        if (user == null) return "login/registration";
        client.createUser(user.getUsername(), user.getPasswordHash());
        return "login/login";
    }

    @RequestMapping("/admin/all_users")
    public String getUsers(@NotNull final Model model) {
        List<UserDTO> users = client.findAllUsers();
        model.addAttribute("entityList", users);
        return "admin/user";
    }

    @GetMapping(value = "/admin/user_update/{id}")
    public String changePassword(
            @NotNull final Model model,
            @PathVariable String id) throws Exception {
        @Nullable final UserDTO user = client.findUserByUsername(id);
        if (user == null) throw new Exception();
        user.setPasswordHash("");
        model.addAttribute(user);
        return "admin/user_update";
    }

    @PostMapping(value = "/admin/user_update/{id}")
    public String changePassword(
            @ModelAttribute @Nullable final UserDTO user
    ) throws Exception {
        if (user == null) throw new Exception();
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty()) return "redirect:/admin/user_update/{id}";
        client.updateUser(user);
        return "redirect:/admin/all_users";
    }

    @GetMapping(value = "/admin/user_remove/{id}")
    public String removeProject(
            @PathVariable final String id
    ) {
        client.deleteUserById(id);
        return "redirect:/admin/all_users";
    }

    @GetMapping("/admin/user/redirect")
    public String redirect() {return "redirect:/admin/all_users";}

}
