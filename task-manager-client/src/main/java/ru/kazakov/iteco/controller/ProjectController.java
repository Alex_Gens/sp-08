package ru.kazakov.iteco.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.client.Client;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.enumeration.Status;
import java.security.Principal;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ProjectController {

    @Autowired
    private Client client;

    @RequestMapping("/all_projects")
    public String getProjects(
            @NotNull final Principal principal,
            @NotNull final Model model
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        List<ProjectDTO> projects = client.getAllProjects(currentUserId);
        model.addAttribute("entityList", projects);
        return "project/project";
    }

    @GetMapping("/project_create")
    public String createProject(@NotNull final Model model) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        model.addAttribute("project", project);
        model.addAttribute("values", Status.values());
        return "project/project_create";
    }

    @PostMapping(value = "/project_create")
    public String addNewProject(
            @NotNull final Principal principal,
            @ModelAttribute("project") @NotNull final ProjectDTO project
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        project.setUserId(currentUserId);
        client.addProject(currentUserId, project);
        return "redirect:/all_projects";
    }

    @GetMapping(value = "/project/{id}")
    public String viewProject(
            @NotNull final Principal principal,
            @NotNull final Model model,
            @PathVariable final String id
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        @Nullable final ProjectDTO project = client.getProject(currentUserId, id);
        if (project == null) return "/404";
        if (!currentUserId.equals(project.getUserId())) return "/404";
        model.addAttribute("project", project);
        return "project/project_view";
    }

    @GetMapping(value = "/project_update/{id}")
    public String updateProject(
            @NotNull final Principal principal,
            @NotNull final Model model,
            @PathVariable final String id
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        @Nullable final ProjectDTO project = client.getProject(currentUserId, id);
        if (project == null) return "/404";
        if (!currentUserId.equals(project.getUserId())) return "/404";
        model.addAttribute("project", project);
        model.addAttribute("values", Status.values());
        return "/project/project_update";
    }

    @PostMapping(value = "/project_update/{id}")
    public String mergeProject(
            @NotNull final Principal principal,
            @ModelAttribute final ProjectDTO dto
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        client.addProject(currentUserId, dto);
        return "redirect:/all_projects";
    }

    @GetMapping(value = "/project_remove/{id}")
    public String removeProject(
            @NotNull final Principal principal,
            @PathVariable final String id
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        client.deleteProject(currentUserId, id);
        return "redirect:/all_projects";
    }

    @GetMapping("/project/redirect")
    public String redirect() {return "redirect:/all_projects";}

}
