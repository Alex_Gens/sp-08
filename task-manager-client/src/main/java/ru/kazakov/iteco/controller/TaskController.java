package ru.kazakov.iteco.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.client.Client;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.enumeration.Status;
import java.security.Principal;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class TaskController {

    @Autowired
    private Client client;

    @RequestMapping("/all_tasks")
    public String getTasks(
            @NotNull final Principal principal,
            @NotNull final Model model
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        List<TaskDTO> tasks = client.getAllTasks(currentUserId);
        model.addAttribute("entityList", tasks);
        return "task/task";
    }

    @GetMapping("/task_create")
    public String createTask(@NotNull final Model model) {
        @NotNull final TaskDTO task = new TaskDTO();
        model.addAttribute("task", task);
        model.addAttribute("values", Status.values());
        return "task/task_create";
    }

    @PostMapping(value = "/task_create")
    public String addNewTask(
            @NotNull final Principal principal,
            @ModelAttribute("task") @NotNull final TaskDTO task
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        task.setUserId(currentUserId);
        client.addTask(currentUserId, task);
        return "redirect:/all_tasks";
    }

    @GetMapping(value = "/task/{id}")
    public String viewTask(
            @NotNull final Principal principal,
            @NotNull final Model model,
            @PathVariable final String id
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        @Nullable final TaskDTO task = client.getTask(currentUserId, id);
        if (task == null) return "/404";
        if (!currentUserId.equals(task.getUserId())) return "/404";
        model.addAttribute("task", task);
        return "task/task_view";
    }

    @GetMapping(value = "/task_update/{id}")
    public String updateTask(
            @NotNull final Principal principal,
            @NotNull final Model model,
            @PathVariable final String id
    ) {
        @NotNull final String currentUsername =  SecurityContextHolder.getContext().getAuthentication().getName();
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        @Nullable final TaskDTO task = client.getTask(currentUserId, id);
        if (task == null) return "/404";
        if (!currentUserId.equals(task.getUserId())) return "/404";
        model.addAttribute("task", task);
        model.addAttribute("values", Status.values());
        return "/task/task_update";
    }

    @PostMapping(value = "/task_update/{id}")
    public String mergeTask(
            @NotNull final Principal principal,
            @ModelAttribute final TaskDTO dto
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        client.addTask(currentUserId, dto);
        return "redirect:/all_tasks";
    }

    @GetMapping(value = "/task_remove/{id}")
    public String removeTask(
            @NotNull final Principal principal,
            @PathVariable final String id
    ) {
        @Nullable final String currentUserId = client.findUserByUsername(principal.getName()).getId();
        if (currentUserId == null || currentUserId.isEmpty()) return "redirect:";
        client.deleteTask(currentUserId, id);
        return "redirect:/all_tasks";
    }

    @GetMapping("/task/redirect")
    public String redirect() {return "redirect:/all_tasks";}

}
