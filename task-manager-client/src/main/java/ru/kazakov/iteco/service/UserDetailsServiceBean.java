package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kazakov.iteco.client.Client;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.enumeration.RoleType;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private Client client;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        @Nullable final UserDTO user = client.findUserByUsername(username);
        if (user == null) throw new UsernameNotFoundException("User not found.");
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPasswordHash());
        final List<RoleType> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (RoleType role: userRoles) roles.add(role.toString());
        builder.roles(roles.toArray(new String[] {}));
        return builder.build();
    }

}
