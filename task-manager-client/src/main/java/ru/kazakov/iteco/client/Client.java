package ru.kazakov.iteco.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.dto.UserDTO;
import java.util.List;

@FeignClient(value = "task-manager-server", url="127.0.0.1:8081")
public interface Client {

    @GetMapping("/projects")
    List<ProjectDTO> getAllProjects(@RequestParam("currentUserId") @NotNull final String currentUserId);

    @GetMapping(value = "/projects/{id}")
    ProjectDTO getProject(@RequestParam("currentUserId") @NotNull final String currentUserId,
                          @PathVariable(value = "id") @Nullable final String id);

    @PostMapping("/projects")
    public ProjectDTO addProject(@RequestParam("currentUserId") @NotNull final String currentUserId,
                                 @Nullable final ProjectDTO dto);

    @DeleteMapping("/projects/{id}")
    public void deleteProject(@RequestParam("currentUserId") @NotNull final String currentUserId,
                              @PathVariable(value = "id") @Nullable final String id);

    @PutMapping("/projects")
    public void updateProject(@RequestParam("currentUserId") @NotNull final String currentUserId,
                              @Nullable final ProjectDTO dto);

    @GetMapping("/tasks")
    List<TaskDTO> getAllTasks(@RequestParam("currentUserId") @NotNull final String currentUserId);

    @GetMapping(value = "/tasks/{id}")
    TaskDTO getTask(@RequestParam("currentUserId") @NotNull final String currentUserId,
                    @PathVariable(value = "id") @Nullable final String id);

    @PostMapping("/tasks")
    public TaskDTO addTask(@RequestParam("currentUserId") @NotNull final String currentUserId,
                           @Nullable final TaskDTO dto);

    @DeleteMapping("/tasks/{id}")
    public void deleteTask(@RequestParam("currentUserId") @NotNull final String currentUserId,
                           @PathVariable(value = "id") @Nullable final String id);

    @PutMapping("/tasks")
    public void updateTask(@RequestParam("currentUserId") @NotNull final String currentUserId,
                           @Nullable final TaskDTO dto);

    @PostMapping(value = "/users/login_user")
    public String login(@RequestParam("currentUserId") @NotNull final String currentUserId,
                        @NotNull final UserDTO dto);


    @PostMapping(value = "/users/logout_user")
    public void logout(@RequestParam("currentUserId") @NotNull final String currentUserId);

    @GetMapping(value = "/users/{username}")
    public UserDTO findUserByUsername(@PathVariable @Nullable final String username);

    @PostMapping("/users/create")
    public void createUser(@RequestParam("username") @NotNull final String username,
                           @RequestParam("password") @NotNull final String password);

    @GetMapping("/admin/all_users")
    public List<UserDTO> findAllUsers();

    @PutMapping("/users/update_user/{id}")
    public void updateUser(@NotNull final UserDTO user);

    @DeleteMapping("/admin/deleteUser/{id}")
    public void deleteUserById(@PathVariable @NotNull final String id);

}
