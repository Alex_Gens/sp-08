<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Project create</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<style>
    h1 {
        border-bottom: 1px dashed black;
    }

    body {
        margin: 0;
        padding: 0;
    }

    input[type="text"], input[type="date"], input[type="number"], input[type="password"] {
        border: 1px solid black;
        border-radius: 3px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -khtml-border-radius: 3px;
        background: #ffffff !important;
        outline: none;
        height: 34px;
        width: 350px;
        font-family: 'RobotoLight', serif;
        color: black;
        font-size: 1.6em;
    }

    button {
        font-size: 1.1em;
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 10px 28px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }
</style>

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td bgcolor="black">&nbsp;</td>
        <td bgcolor="black">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color: white; " bgcolor="black">
                <tbody><tr>
                    <td nowrap="nowrap" height="50">
                        <a href="/" target="_blank" style="color: white; text-decoration: none;">MAIN</a>
                        <a style="color: white; text-decoration: none;">|</a>
                        <a href="/all_projects" style="color: white; text-decoration: none;">PROJECTS</a>
                        <a style="color: white; text-decoration: none;">|</a>
                        <a href="/all_tasks" style="color: white; text-decoration: none;">TASKS</a>
                    </td>
                    <td align="right" height="50">
                        <c:if test="${isAuth == true}">
                            <a href="${pageContext.request.contextPath}/logout" style="color: white; text-decoration: none;">LOGOUT</a>
                        </c:if>
                        <c:if test="${isAuth == false}">
                            <a href="${pageContext.request.contextPath}/login" style="color: white; text-decoration: none;">LOGIN</a>
                        </c:if>
                    </td>
                </tr>
                </tbody></table>
        </td>
        <td bgcolor="black">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    </tbody>
</table>
<table class="container" align="center" cellpadding="10">
    <form method="POST" action="${contextPath}/login" class="form-signin">
        <h2 align="center" class="form-heading">Log in</h2>
        <tr class="form-group ${error != null ? 'has-error' : ''}">
            <td>
            <input name="username" type="text" class="form-control" placeholder="Username"
                   autofocus="true"/>
            </td>
        </tr>
        <tr class="form-group ${error != null ? 'has-error' : ''}">
            <td>
            <input name="password" type="password" class="form-control" placeholder="Password"/>
            <span>${error}</span>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </td>
        </tr>
        <tr class="form-group ${error != null ? 'has-error' : ''}">
            <td>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
            <h4 class="text-center"><a href="${contextPath}/registration">Create an account</a></h4>
            </td>
        </tr>
        <tr>
            <td>
                <span>${message}</span>
            </td>
        </tr>
    </form>
</table>
</body>
</html>