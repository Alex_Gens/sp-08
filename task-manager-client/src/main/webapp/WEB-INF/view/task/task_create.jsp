<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html
><head>
    <title>Task create</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<style>
    h1 {
        border-bottom: 1px dashed black;
    }

    body {
        margin: 0;
        padding: 0;
    }

    input[type="text"], input[type="date"], input[type="number"], input[type="password"] {
        border: 1px solid black;
        border-radius: 3px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -khtml-border-radius: 3px;
        background: #ffffff !important;
        outline: none;
        height: 34px;
        width: 350px;
        font-family: 'RobotoLight', serif;
        color: black;
        font-size: 1.6em;
    }

    button {
        font-size: 1.1em;
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 10px 28px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }
</style>

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td bgcolor="black">&nbsp;</td>
        <td bgcolor="black">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color: white; " bgcolor="black">
                <tbody><tr>
                    <td nowrap="nowrap" height="50">
                        <a href="/" target="_blank" style="color: white; text-decoration: none;">MAIN</a>
                        <a style="color: white; text-decoration: none;">|</a>
                        <a href="/all_projects" style="color: white; text-decoration: none;">PROJECTS</a>
                        <a style="color: white; text-decoration: none;">|</a>
                        <a href="/all_tasks" style="color: white; text-decoration: none;">TASKS</a>
                    </td>
                    <td align="right" height="50">
                        <a href="${pageContext.request.contextPath}/logout" style="color: white; text-decoration: none;">LOGOUT</a>
                    </td>
                </tr>
                </tbody></table>
        </td>
        <td bgcolor="black">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <table width="100%" height="100" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; margin-left: 40px;">
            <tbody>
            <tr>
                <td width="100%" style="overflow: hidden;">
                    <h1 style="color: black; border-bottom: none; margin: 0; padding: 0;">TASK CREATE</h1>
                </td>
            </tr>
            </tbody>
        </table>
    </tr>
    <form:form method="POST" modelAttribute="task">
    <form:hidden path="id"/>
    <form:hidden path="dateCreate"/>
    <form:hidden path="userId"/>
    <tr>
        <table width="30%" cellspacing="0" cellpadding="5" border="0" style="border-collapse: collapse; margin-left: 40px;">
            <tbody>
            <tr>
                <td>
                    NAME
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <form:input path="name"/>
                </td>
            </tr>
            <tr>
                <td>
                    DESCRIPTION
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <form:input path="description"/>
                </td>
            </tr>
            <tr>
                <td>
                    DATE START
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <form:input path="dateStart"/>
                </td>
            </tr>
            <tr>
                <td>
                    DATE FINISH
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <form:input path="dateFinish"/>
                </td>
            </tr>
            <tr>
                <td>
                    PROJECT
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <form:select path="projectId">
                        <form:option value="${null}" label="---SELECT---"/>
                        <form:options items="${projects}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </td>
            </tr>
            <tr>
                <td>
                    STATUS
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <form:select path="status" value="${task.status}">
                        <c:forEach var="status_var" items="${values}">
                            <option>${status_var}</option>
                        </c:forEach>
                    </form:select>
                </td>
            </tr>
            </tbody>
        </table>

    </tr>
    <table width="20%" cellspacing="0" cellpadding="10" border="0" style="border-collapse: collapse; margin-bottom: 20px; margin-left: 40px;">
        <tbody>
        <tr>
            <form action="${pageContext.request.contextPath}/task">
                <td colspan="1" align="left">
                    <button type="submit">CREATE</button>
                </td>
            </form>
        </tr>
        </tbody>
    </table>
        </td>
        <td width="20" nowrap="nowrap">&nbsp;</td>
    </tr>
    </tbody></table></td>
</tr>
</form:form>
</tbody></table>

</body></html>
